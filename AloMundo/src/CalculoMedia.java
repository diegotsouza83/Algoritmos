import java.util.Scanner;

public class CalculoMedia {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nota1;
		int nota2;
		int media;
		
		System.out.println("Digite a primeira nota");
		nota1 = scanner.nextInt();
		
		System.out.println("Digite a segunda nota");
		nota2 = scanner.nextInt();
		
		media = (nota1 + nota2) / 2;
		
		
		System.out.println("A média é: " + media);
		
		
	}

}
