import java.util.Scanner;

public class SomaMatrizes {
	
	public static void main(String[] args) {
		int [][]matriz1 = new int[2][2];
		int [][]matriz2 = new int[2][2];
		int [][]resultado = new int[2][2];
		Scanner s = new Scanner(System.in);
		
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.println("Matriz 1 - Preencha a posição " + i + ", " + j);
				matriz1[i][j] = s.nextInt();
			}
		}
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.println("Matriz 2 - Preencha a posição " + i + ", " + j);
				matriz2[i][j] = s.nextInt();
			}
		}
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				resultado[i][j] = matriz1[i][j] + matriz2[i][j];
				System.out.println("Matriz Resultado - " + resultado[i][j]  );

			}
		}
		
	}

}
