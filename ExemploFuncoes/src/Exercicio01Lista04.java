import java.util.Scanner;

public class Exercicio01Lista04 {
	
	 public static void main(String[] args) {
		int numeros[] = new int[10];
		Scanner s = new Scanner(System.in);
		int maiorPar= 0;
		int menorImpar = 0;
		int somatorio = 0;
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Digite um número");
			int numero = s.nextInt();
			if (numero % 2 == 0) {
				maiorPar = numero;
			}else {
				menorImpar = numero;
			}
			
			numeros[i] = numero;
		}
		
		for (int i = 0; i < numeros.length; i++) {
			somatorio = somatorio + numeros[i];
			if (numeros[i] % 2 == 0 && numeros[i] > maiorPar){
				maiorPar = numeros[i];
			} else if (numeros[i] % 2 != 0 && numeros[i] < menorImpar){
				menorImpar = numeros[i];
			}
		}
		
		if (maiorPar == 0) {
			System.out.println("Não existe elemento par");
		} else {
			System.out.println("Maior par = " + maiorPar);
		}
		
		if (menorImpar == -2) {
			System.out.println("Não existe elemento ímpar");
		} else {
			System.out.println("Menor ímpar = " + menorImpar);
		}
		
		System.out.println("Somatório = " + somatorio);
		System.out.println("Média = " + somatorio/numeros.length);
		
	}

}
