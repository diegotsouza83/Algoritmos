import java.util.Scanner;

public class Funcoes2 {
	
	static int i;
	
	public static void main(String[] args) {
		int valor1;
		int valor2;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o valor 1");
		valor1 = s.nextInt();
		System.out.println("Digite o valor 2");
		valor2 = s.nextInt();
		
		int resultado = soma(valor1, valor2);
		
		System.out.println("O resultado é : " + resultado);
		
		
	}
	
	static int soma(int numero1, int numero2){
		int resultado = numero1 + numero2;
		return resultado;
	}
	
	static int subtracao(int valor1, int valor2){
		int resultado = valor1 - valor2;
		return resultado;
	}

}
