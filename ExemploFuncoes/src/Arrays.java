import java.util.Scanner;

public class Arrays {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int valores[] = new int[100];
		
		for (int i = 0; i < valores.length; i++) {
			System.out.println("Digite um valor");
			valores[i] = s.nextInt();
		}
		
		int cont = 0;
		int soma = 0;
		int maiorValor = valores[0];
		
		while (cont < valores.length){
			if (valores[cont] > maiorValor){
				maiorValor = valores[cont];
			}
			soma = soma + valores[cont];
			cont++;
		}
		System.out.println("Soma = " + soma);
		System.out.println("Mádia = " + (soma/valores.length));
		System.out.println("maior valor = " + maiorValor);
	}

}
