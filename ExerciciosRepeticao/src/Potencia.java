import java.util.Scanner;

public class Potencia {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o valor de N");
		int n = s.nextInt() ;
		System.out.println("Digite o valor de M");
		int m = s.nextInt();
		
		int cont = 1;
		int pot = n;
		cont = 1;
		while (cont < m) {
			pot = pot * n;
			cont++;
		}
		
		System.out.println("Resultado = " + pot);
	}

}
