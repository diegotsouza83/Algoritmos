import java.util.Scanner;

/*
 * Faça um algoritmo que leia a velocidade máxima permitida em uma avenida e a velocidade com que o 
 * motorista estava dirigindo nela. Em seguida, calcule a multa  que uma pessoa vai receber.  
 * Sabendo que são pagos: 
         a) 50 reais se o motorista ultrapassar em até 10km/h a velocidade permitida;
         b) 100 reais, se o motorista ultrapassar de 11 a 30 km/h a velocidade permitida. 
         c) 200 reais, se estiver acima de 30km/h da velocidade permitida. 
 O programa deverá imprimir como saída a velocidade do veículo, a velocidade permitida e o 
 valor da multa. 

 */
public class Exercicio05 {
	
	public static void main(String[] args) {
		double velocidadeAtual;
		double velocidadePermitida;
		double valorDaMulta = 0;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a velocidade atual");
		velocidadeAtual = s.nextDouble();
		
		System.out.println("Digite a velocidade permitida");
		velocidadePermitida = s.nextDouble();
		
		double diferenca = velocidadeAtual - velocidadePermitida;
		
		if (diferenca > 0 && diferenca <= 10){
			valorDaMulta = 50;
		} else if (diferenca > 10 && diferenca <= 30){
			valorDaMulta = 100;
		} else{
			valorDaMulta = 200;
		}
		
		
		if (diferenca > 0) {
			System.out.println("Voce ultrapassou a velocidade permitida em " + diferenca + " Km. Pagará multa de R$ " + valorDaMulta);
		}else {
			System.out.println("Parabéns. Você não ultrapassou a velocidade permitida");
		}
	}

}
