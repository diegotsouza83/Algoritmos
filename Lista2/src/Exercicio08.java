import java.util.Scanner;


/*
 * Calcular a média final dadas as notas de 3 provas e produzir uma saída com a média e a
 *  situação do aluno de acordo com o seguinte critério: 
 *  média >= 7, aprovado; 3 <= média < 7, recuperação; média < 3, reprovado. 
 *  Se o aluno se encontrar em recuperação, solicitar a nota da quarta prova e, após calcular a 
 *  média final, informar se o aluno passou (média final >=5) ou não.
 */
public class Exercicio08 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		double nota1;
		double nota2;
		double nota3;
		double media;
		
		System.out.println("Digite a nota 1");
		nota1 = s.nextDouble();
		
		System.out.println("Digite a nota 2");
		nota2 = s.nextDouble();
		
		System.out.println("Digite a nota 3");
		nota3 = s.nextDouble();
		
		media = (nota1 + nota2 + nota3) / 3;
		
		if (media >= 7) {
			System.out.println("Aprovado por média. Media = " + media);
		} else if (media >= 3) {
			double notaProvaFinal;
			double mediaFinal;
			
			System.out.println("Digite a nota da final");
			notaProvaFinal = s.nextDouble();
			
			mediaFinal = (media + notaProvaFinal) / 2;
			
			if (mediaFinal >= 5) {
				System.out.println("Aprovado na final. Média = " + mediaFinal);
			} else {
				System.out.println("Reprovado na final. Média = " + mediaFinal);
			}
			
			
		} else {
			System.out.println("Reprovado. Média = " + media);
		}
		
	}

}
