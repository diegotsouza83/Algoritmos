import java.util.Scanner;

/*
 * Ler dois números inteiros, x e y, imprimir o quociente e o resto da divisão inteira entre eles.
 */
public class Exercicio03 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		int valor1;
		int valor2;
		
		System.out.println("Digite o valor 1");
		valor1 = s.nextInt();
		
		System.out.println("Digite o valor 2");
		valor2 = s.nextInt();
		
		if (valor2 != 0) {
			System.out.println("O quociente é " + (valor1/valor2));
			System.out.println("O resto é " + (valor1 % valor2));
		} else {
			System.out.println("Divisão por zero");
		}
		
	}

}
