import java.util.Scanner;

public class Conjuntos {
	
	public static void main(String[] args) {
		int tamanho;
		int v1[];
		int v2[];
		int resultado[];
		int qtd = 0;
		int aux = 0;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o tamanho dos conjuntos");
		tamanho = s.nextInt();
		
		v1 = new int[tamanho];
		v2 = new int[tamanho];
		
		for (int i = 0; i < tamanho; i++) {
			System.out.println("Digiteo valor " + (i + 1) + " do conjunto 1");
			v1[i] = s.nextInt();
			System.out.println("Digiteo valor " + (i + 1) + " do conjunto 2");
			v2[i] = s.nextInt();
		}
		
		for (int i = 0; i < v1.length; i++) {
			for (int j = 0; j < v2.length; j++) {
				if (v1[i] == v2[j]){
					qtd++;
				}
			}
		}
		
		System.out.println("Interseção = " + qtd);
		
		if (qtd > 0) {
			resultado = new int[qtd];
			for (int i = 0; i < v1.length; i++) {
				for (int j = 0; j < v2.length; j++) {
					if (v1[i] == v2[j]){
						resultado[aux] = v1[i];
						aux++;
					}
				}
			}
			
			for (int i = 0; i < resultado.length; i++) {
				System.out.println("valor [" + i + "] = " + resultado[i]);
			}
		} else{
			System.out.println("Nenhum elemento na interseção");
		}
	}

}
